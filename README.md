# README

### What is this repository for?

- This project is for setting up the React 17 with Webpack 5.

### How do I get set up?

To run the application,

- Clone the folder.
- Go to the folder on your terminal.
- Run `npm i/npm install`. or `yarn add`
- Next, run `npm start` or `yarn start` (depending on the package manager used)
