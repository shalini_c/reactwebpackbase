const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const paths = require('./paths');

module.exports = {
    // Where webpack looks to start building the bundle
    entry: [paths.src + '/index.js'],

    // Where webpack outputs the assets and bundles
    output: {
        path: paths.build,
        filename: '[name].[contenthash].js',
        publicPath: '/',
        pathinfo: false
    },

    // Customize the webpack build process
    plugins: [
        // Removes/cleans build folders and unused assets when rebuilding
        new CleanWebpackPlugin(),

        // Copies files from target to destination folder
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: paths.public,
                    to: 'assets',
                    globOptions: {
                        ignore: ['*.DS_Store']
                    },
                    noErrorOnMissing: true
                }
            ]
        }),

        // Generates an HTML file from a template
        new HtmlWebpackPlugin({
            template: paths.src + '/index.html', // template file
            filename: 'index.html' // output file
            // favicon: './src/favicon.png'
        }),

        new CompressionPlugin()
    ],

    // Determine how modules within the project are treated
    module: {
        rules: [
            // JavaScript: Use Babel to transpile JavaScript files
            {
                test: /\.(js|jsx)$/,
                // exclude: /node_modules/,
                include: paths.src,
                loader: 'babel-loader',
                options: {
                    plugins: [
                        //needed for antD
                        ['import', { libraryName: 'antd', style: true }] //needed for antD
                    ]
                }
            },

            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader'
                    // {
                    //     loader: 'less-loader',
                    //     options: {
                    //         lessOptions: {
                    //             javascriptEnabled: true //needed for antD
                    //         }
                    //     }
                    // }
                ]
            },

            // Images: Copy image files to build folder
            { test: /\.(?:ico|gif|png|jpe?g)$/i, type: 'asset/resource' },

            // Fonts and SVGs: Inline files
            { test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: 'asset/inline' }
        ]
    },

    performance: {
        hints: 'warning',
        // Calculates sizes of gziped bundles.
        assetFilter: function (assetFilename) {
            return assetFilename.endsWith('.js.gz');
        }
    },

    resolve: {
        modules: [paths.src, 'node_modules'],
        extensions: ['.js', '.jsx', '.json']
    }
};
